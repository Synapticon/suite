const fs = require('fs')
const {
    exec
} = require("child_process")

const {
    validators
} = require(`${process.cwd()}/config/remote.json`)

for (let i = 0; i < validators.length; i++) {

    let chain = validators[i].chain

    fs.access(`${process.cwd()}/keys/${chain}/validator_${i}_rsa`, fs.F_OK, (notExists) => {

        if (notExists) {
            exec(`ssh-keygen -a 1000 -b 4096 -P "" -C "" -E sha256 -f ${process.cwd()}/keys/${chain}/validator_${i}_rsa -o -t rsa`, (error, _, stderr) => {
                if (error) {
                    console.log(`error: ${error.message}`)
                    return
                }
                if (stderr) {
                    console.log(`stderr: ${stderr}`)
                    return
                }

                exec(`chmod 600 ${process.cwd()}/keys/${chain}/validator_${i}_rsa`, (error, _, stderr) => {
                    if (error) {
                        console.log(`error: ${error.message}`)
                        return
                    }
                    if (stderr) {
                        console.log(`stderr: ${stderr}`)
                        return
                    }
                })

            })

        }
    })
}